// Copyright 2023 Kevin Zuern. All rights reserved.

package weather

import (
	"bytes"
	"context"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/zuern/envcanadaforecast/types"
	"golang.org/x/text/encoding/charmap"
)

const baseURL = "https://dd.weather.gc.ca/citypage_weather/xml/"

type Client struct {
	*http.Client
}

// NewClient creates a new weather client, optionally wrapping an existing http
// client or using http.DefaultClient if none is provided.
func NewClient(client ...*http.Client) *Client {
	if len(client) > 1 {
		return &Client{client[0]}
	}
	return &Client{http.DefaultClient}
}

// GetWeather fetches a weather forecast for the given location. Optional
// parameter french if true will return a forecast in French.
func (c *Client) GetWeather(cx context.Context, loc types.Location, french ...bool) (*types.Weather, error) {
	lang := "e"
	if len(french) > 0 && french[0] {
		lang = "f"
	}
	URL := fmt.Sprintf("%s%s_%s.xml", baseURL, string(loc), lang)
	req, err := http.NewRequestWithContext(cx, "GET", URL, nil)
	if err != nil {
		return nil, err
	}
	var resp *http.Response
	if resp, err = c.Do(req); err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var body []byte
	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	}
	switch resp.StatusCode {
	case 200:
		// OK
	case 404:
		return nil, fmt.Errorf("weather: location not found")
	default:
		return nil, fmt.Errorf("weather: api returned http code %d", resp.StatusCode)
	}
	buf := bytes.NewBuffer(body)
	decoder := xml.NewDecoder(buf)
	// Convert xml charset to utf-8.
	decoder.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
		// https://stackoverflow.com/questions/34712015/unmarshal-multiple-xml-items/34712322#34712322
		if charset == "ISO-8859-1" {
			// Windows-1252 is a superset of ISO-8859-1, so should do here
			return charmap.Windows1252.NewDecoder().Reader(input), nil
		}
		return nil, fmt.Errorf("unexpected charset: %s", charset)
	}
	var weather types.Weather
	err = decoder.Decode(&weather)
	return &weather, err
}
