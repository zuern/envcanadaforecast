// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type HistoricMeasurement struct {
	Measurement
	Class  string `xml:"class,attr"`
	Period string `xml:"period,attr"`
	Year   int    `xml:"year,attr"`
}
