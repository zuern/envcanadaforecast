// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type Forecast struct {
	Period              string `xml:"textForecastName,attr"`
	TextSummary         string `xml:"textSummary"`
	CloudPrecip         string `xml:"cloudPrecip>textSummary"`
	AbbreviatedForecast struct {
		IconCode int `xml:"iconCode"`
		// POPPercentage Possibility of Precipitation Percentage.
		POPPercentage float64 `xml:"pop"`
		TextSummary   string  `xml:"textSummary"`
	} `xml:"abbreviatedForecast"`
	Temperatures struct {
		TextSummary string      `xml:"textSummary"`
		Temperature Measurement `xml:"temperature"`
	} `xml:"temperatures"`
	// TODO Winds.
	Precipitation struct {
		TextSummary string `xml:"textSummary"`
		PrecipType  struct {
			Start int    `xml:"start,attr"`
			End   int    `xml:"end,attr"`
			Value string `xml:",chardata"`
		} `xml:"precipType"`
	} `xml:"precipitation"`
}

type HourlyForecast struct {
	DateTimeUTC string      `xml:"dateTimeUTC,attr"`
	IconCode    string      `xml:"iconCode"`
	Temperature Measurement `xml:"temperature"`
	// TODO Windchill
	Wind Wind `xml:"wind"`
}
