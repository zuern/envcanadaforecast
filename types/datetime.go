// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type DateTime struct {
	Name      string `xml:"name,attr"`
	Zone      string `xml:"zone,attr"`
	UTCOffset int    `xml:"UTCOffset,attr"`
	Year      int    `xml:"year"`
	Month     int    `xml:"month"`
	Day       int    `xml:"day"`
	Hour      int    `xml:"hour"`
	Minute    int    `xml:"minute"`
	TimeStamp string `xml:"timeStamp"`
}
