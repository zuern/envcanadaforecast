// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type ForecastGroup struct {
	DateTimes []DateTime `xml:"dateTime"`
	Forecasts []Forecast `xml:"forecast"`
}

type HourlyForecastGroup struct {
	DateTimes       []DateTime       `xml:"dateTime"`
	HourlyForecasts []HourlyForecast `xml:"hourlyForecast"`
}
