// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type Almanac struct {
	Temperature   []HistoricMeasurement `xml:"temperature"`
	Precipitation []HistoricMeasurement `xml:"precipitation"`
	// POPPercentage Possibility of Precipitation Percentage.
	POPPercentage float64 `xml:"pop"`
}
