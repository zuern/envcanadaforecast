## [0.1.1] - 2023-02-25
### Changed
- Refreshed list of locations

## [0.1.0] - 2021-02-09
### Added
- Location map to map a city name string to a location id.

