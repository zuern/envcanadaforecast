// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type Unit struct {
	UnitType string `xml:"unitType,attr"`
	Units    string `xml:"units,attr"`
}

type Measurement struct {
	Unit
	Value float64 `xml:",chardata"`
}
