// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type RiseSet struct {
	Disclaimer string     `xml:"disclaimer"`
	DateTimes  []DateTime `xml:"dateTimes"`
}
