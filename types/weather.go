// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type Weather struct {
	License  string `xml:"license"`
	Location struct {
		Region string `xml:"region"`
	} `xml:"location"`
	CurrentConditions CurrentConditions `xml:"currentConditions"`
	ForecastGroup     struct {
		ForecastGroup   `xml:"forecastGroup"`
		RegionalNormals struct {
			TextSummary string        `xml:"textSummary"`
			Temperature []Measurement `xml:"temperature"`
		} `xml:"regionalNormals"`
	} `xml:"forecastGroup"`
	HourlyForecastGroup HourlyForecastGroup `xml:"hourlyForecastGroup"`
	RiseSet             RiseSet             `xml:"riseSet"`
	Almanac             Almanac             `xml:"almanac"`
}
