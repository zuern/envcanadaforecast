// Copyright 2023 Kevin Zuern. All rights reserved.

package weather_test

import (
	"context"
	"testing"
	"time"

	weather "gitlab.com/zuern/envcanadaforecast"
	"gitlab.com/zuern/envcanadaforecast/types"
)

func TestGetWeather(t *testing.T) {
	c := weather.NewClient()
	cx, cf := context.WithTimeout(context.Background(), 3*time.Second)
	defer cf()
	toronto, err := c.GetWeather(cx, types.LocToronto, false)
	if err != nil {
		t.FailNow()
	}
	if toronto == nil {
		t.FailNow()
	}
	if len(toronto.CurrentConditions.Condition) == 0 {
		t.FailNow()
	}
	// Get the French-language forecast.
	toronto, err = c.GetWeather(cx, types.LocToronto, true)
	if err != nil {
		t.FailNow()
	}
	if toronto == nil {
		t.FailNow()
	}
	if len(toronto.CurrentConditions.Condition) == 0 {
		t.FailNow()
	}
}
