// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type CurrentConditions struct {
	Station struct {
		Code string `xml:"code,attr"`
		Name string `xml:",chardata"`
	} `xml:"station"`
	Observation []DateTime  `xml:"dateTime"`
	Condition   string      `xml:"condition"`
	IconCode    int         `xml:"iconCode"`
	Temperature Measurement `xml:"temperature"`
	Wind        Wind        `xml:"wind"`
}
