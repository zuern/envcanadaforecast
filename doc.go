// Package weather provides a client for accessing current weather information
// from Environment Canada.
package weather
