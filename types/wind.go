// Copyright 2023 Kevin Zuern. All rights reserved.

package types

type Wind struct {
	Speed     Measurement `xml:"speed"`
	Gust      Measurement `xml:"gust"`
	Direction string      `xml:"direction"`
}
