// Package weather provides a strictly-typed representation of a location's
// weather information.
package types

//go:generate go run ../gen/genlocations.go -out locations.go
